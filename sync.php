<?php
	require 'vendor/autoload.php';
	
	use FacebookAds\Api;	
	use FacebookAds\Object\LeadgenForm;
	use FacebookAds\Object\Page;
	use FacebookAds\Object\Fields\AdReportRunFields;

	global $full_name;
	global $first_name;
	global $last_name;
	global $date_of_birth;
	global $zip;
	global $email;

	date_default_timezone_set('UTC');
	set_time_limit(0); 
	$sendCount = 0;

	$conn = new PDO("");

	$now				= strtotime('now');
	$start_date			= strtotime('2018-05-01 00:00:00');

	//FB Form IDs to retrieve with State and Age-group,eg:
	//	$forms = ["123456769" => ["WA", "51+"],	"11122233444" => ["ID", "25-34"], "9998887771121" => ["CT", "ALL"]];

	$forms = [
			"" => ["WA", "51+"]
		];
	
	
	//init(app_id, app_secret, token)
	Api::init('', '', '');
	
	/*debug*/
	// var_dump($leadgen_forms);
	// $page = new Page();
	// $leadgen_forms = $page->getLeadgenForms();

	$first_name = "";
	$last_name = "";
	$zip = "";
	$email = "";
	
	foreach($forms as $FBform=>$values){
		echo "Evaluating FB Form: " . $FBform . PHP_EOL;
		
		$form = new LeadgenForm($FBform);
		
		$leads = $form->getLeads(array(), array(
		  AdReportRunFields::FILTERING => array(
			array(
			  'field' => 'time_created',
			  'operator' => 'GREATER_THAN',
			  'value' => $start_date
			),
			array(
			  'field' => 'time_created',
			  'operator' => 'LESS_THAN',
			  'value' => $now,
			),
		  ),
		));
		$leads->setUseImplicitFetch(true);
		
		$fbids = array();
		foreach ($leads as $item) {
			//var_dump($item);

			if(!in_array($item->id, $fbids)){
				array_push($fbids, $item->id);
				setFields($item->field_data);
				
				$sql = "INSERT INTO [dbo].[Lead]
				   ([full_name]
				   ,[date_of_birth]
				   ,[age_range]
				   ,[state]
				   ,[zip]
				   ,[email]
				   ,[submitdate]
				   ,[fbid]
				   ,[formid])
					SELECT
				   " . $conn->quote($full_name) . "
				   ," . $conn->quote($date_of_birth) . "
				   ," . $conn->quote($values[1]) . "
				   ," . $conn->quote($values[0]) . "
				   ," . $conn->quote(substr($zip,0,5)) . "
				   ," . $conn->quote($email) . "
				   ,CONVERT(DATETIME," . date("'Y-m-d H:i:s'", strtotime($item->created_time)) . ")
				   ," . $conn->quote($item->id) ."
				   ," . $conn->quote($FBform) ."
				   WHERE ". $conn->quote($email) . " not in (select email from lead);";
				   			
				$sth = $conn->prepare($sql);
				$sth->execute();
				$err = $sth->errorinfo();
				
				if($err[0] != 00000 ){  
					echo "sql error: " . print_r($sth->errorinfo(), true);
					var_dump($sth);
					die();
				}
			} else {
				echo "Duplicate FBID skipping." . PHP_EOL;
			}
		}		
	}

	function setFields($field_data){
		
		foreach($field_data as $field){ 
			//var_export($field["name"]);

			switch ($field["name"]) {
				case "full_name":
					$full_name = $field["values"][0];
					break;
				case "first_name":
					$first_name = $field["values"][0];
					break;
				case "last_name":
					$last_name = $field["values"][0];
					break;
				case "date_of_birth":
					$date_of_birth = $field["values"][0];
					break;
				case "zip_code":
					$zip = $field["values"][0];
					break;
				case "email":
					$email = $field["values"][0];
					break;				
			}
		}
	}

?>