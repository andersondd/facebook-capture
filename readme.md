URL to update user token:
https://developers.facebook.com/tools/debug/accesstoken/?access_token=[current-long-token]

URL to generate new token, then Extend:
https://developers.facebook.com/tools/accesstoken/


## General Steps
1. Setup FB developer account
2. Create an app and leave in dev mode
3. Populate script Api::init() with Application ID, App Secret, and User or App token with proper permissions (Page Admin) to dowload form data
4. Add form IDS in $forms array
5. Modify start and end dates as needed
6. Customize as needed

## Specific Deliverable
- A csv with the output from FB Lead Form. 
- Data is different than downloaded file directly from FB because it does not have field prefixes. Eg. the 'l:' in fbid) "l:123456789434"
- CSV defined as: [full_name],[date_of_birth],[age_range],[state],[zip],[email],[submitdate],[fbid],[formid]
- Deliver to Anderson SFTP account: Folder of your choice, File name reccomened to have both date and time eg. name_YYYYMMDD-HHMMSS.csv
- Occatinal duplication is ok, it will be supressed on import

